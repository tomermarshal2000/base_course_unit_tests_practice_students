package AIF.AerialVehicles;

public interface AerialIntelligenceVehicle {
    String collectIntelligence();
}
