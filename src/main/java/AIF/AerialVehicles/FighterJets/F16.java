package AIF.AerialVehicles.FighterJets;

import AIF.AerialVehicles.AerialBdaVehicle;
import AIF.Missions.AttackMission;
import AIF.Missions.BdaMission;
import AIF.Missions.Mission;
import AIF.Missions.MissionTypeException;

public class F16 extends FighterJet implements AerialBdaVehicle {
    String cameraType;

    public F16(String cameraType, int amountOfMissile, String missileModel, String pilotName, Mission mission, int hoursOfFlightSinceLastRepair, Boolean readyToFly) {
        super(amountOfMissile, missileModel, pilotName, mission, hoursOfFlightSinceLastRepair, readyToFly);
        this.cameraType = cameraType;
    }

    @Override
    public String preformBda() {
        String message = this.getPilotName() + ": " + this.getClass().getSimpleName() +
                " taking pictures of " + ((BdaMission) this.getMission()).getObjective() +
                " with: " + this.getCameraType() + " camera";
        System.out.println(message);
        return message;
    }

    @Override
    public void setMission(Mission mission) throws MissionTypeException {
        if (mission instanceof BdaMission || mission instanceof AttackMission) {
            this.mission = mission;
        } else {
            throw new MissionTypeException("this vehicle cannot preform this mission");
        }
    }

    public String getCameraType() {
        return this.cameraType;
    }
}