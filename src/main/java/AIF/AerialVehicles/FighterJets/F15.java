package AIF.AerialVehicles.FighterJets;

import AIF.AerialVehicles.AerialIntelligenceVehicle;
import AIF.Missions.AttackMission;
import AIF.Missions.IntelligenceMission;
import AIF.Missions.Mission;
import AIF.Missions.MissionTypeException;

public class F15 extends FighterJet implements AerialIntelligenceVehicle {
    String sensorType;

    public F15(String sensorType, int amountOfMissile, String missileModel, String pilotName, Mission mission, int hoursOfFlightSinceLastRepair, Boolean readyToFly) {
        super(amountOfMissile, missileModel, pilotName, mission, hoursOfFlightSinceLastRepair, readyToFly);
        this.sensorType = sensorType;
    }

    @Override
    public String collectIntelligence() {
        String message = getPilotName() + ": " + this.getClass().getSimpleName() +
                " Collecting Data in " + ((IntelligenceMission) this.getMission()).getRegion() +
                " with sensor type: " + getSensorType();
        System.out.println(message);
        return message;
    }

    @Override
    public void setMission(Mission mission) throws MissionTypeException {
        if (mission instanceof IntelligenceMission || mission instanceof AttackMission) {
            this.mission = mission;
        } else {
            throw new MissionTypeException("this vehicle cannot preform this mission");
        }
    }

    public String getSensorType() {
        return sensorType;
    }


    @Override
    public String toString() {
        return "F15{" + '\n' +
                "sensorType='" + sensorType + '\n' +
                ", amountOfMissile=" + amountOfMissile +
                ", missileModel='" + missileModel + '\n' +
                ", pilotName='" + pilotName + '\n' +
                ", mission=" + mission +
                ", hoursOfFlightSinceLastRepair=" + hoursOfFlightSinceLastRepair + '\n' +
                ", readyToFly=" + readyToFly + +'\n' + '\n' +
                '}';
    }
}