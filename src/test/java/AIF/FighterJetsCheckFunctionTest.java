package AIF;

//incomplete ...


import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class FighterJetsCheckFunctionTest {
    public static AIFUtil aifUtil;

    @BeforeClass
    public static void setUpBeforeClass() {
        aifUtil = new AIFUtil();
    }

    @Test//23
    public void testCheckFighterJetsEquilibriumGroupsInGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"), 23);
        assertEquals("failure - hoursOfFlightSinceLastRepair = 0 should  0 f15.check().", 23, aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair());
    }

    @Test//-6
    public void testCheckFighterJetsEquilibriumGroupsUnderGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"), -6);
        assertEquals("failure - hoursOfFlightSinceLastRepair = -6 should be reset to 0 by f15.check().", 0, aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair());
    }

    @Test//299
    public void testCheckFighterJetsEquilibriumGroupsAboveGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"), 299);//1444 - arbitrary
        assertEquals("failure - hoursOfFlightSinceLastRepair = 144 shouldn't be reset to 0 after f15.check().", 0, aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair());
    }

    //ערכי גבול
    @Test
    public void testCheckLimitsFighterJetsLowerBoundLeft() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"), -1);
        assertEquals("failure - hoursOfFlightSinceLastRepair = -1 should throws an exception ? or reset to zero ?.", 0, aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundRight() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"), 1);
        assertEquals("failure - hoursOfFlightSinceLastRepair = 1 should be reset to 1 f15.check().", 1, aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void testCheckLimitsFighterJetsUpperBoundLeft() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"), 249);
        assertEquals("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 f15.check().", 249, aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void testCheckUniqueEndCasesFighterJets() {

    }
}