package AIF;

import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class F15SetMissionFunctionTest {
    public static AIFUtil aifUtil;

    @BeforeClass
    public static void setUpBeforeClass() {
        aifUtil = new AIFUtil();
    }

    @Test//Executable Mission -> IntelligenceMission
    public void testF15ExecutableMission() {
        try {
            aifUtil.getAerialVehiclesHashMap().get("F15").setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        } catch (MissionTypeException missionTypeException) {//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();

        }
    }

    @Test//Executable Mission -> AttackMission
    public void testF15UnexecutableMission() {
        try {
            aifUtil.getAerialVehiclesHashMap().get("F15").setMission(aifUtil.getAllMissions().get("bda"));
            fail();
        } catch (MissionTypeException missionTypeException) {
            assertTrue(true);
        }
    }
}