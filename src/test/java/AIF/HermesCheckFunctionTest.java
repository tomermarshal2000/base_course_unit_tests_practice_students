package AIF;

//incomplete ...

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class HermesCheckFunctionTest {
    public static AIFUtil aifUtil;

    @BeforeClass
    public static void setUpBeforeClass() {
        aifUtil = new AIFUtil();
    }

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();//init  aerial vehicles and missions.
    }


    //קבוצות שקילות
    @Test//78
    public void testCheckHermesEquilibriumGroupsInGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"), 78);//84 - arbitrary
        assertEquals("failure - hoursOfFlightSinceLastRepair = 84 shouldn't be reset to 0 after Kohav.check().", 78, aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair());
    }


    @Test//299
    public void testCheckHermesEquilibriumGroupsAboveGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"), 123);
        assertEquals("failure - hoursOfFlightSinceLastRepair = 0 should  0 Kohav.check().", 0, aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair());
    }


    //ערכי גבול

    @Test
    public void testCheckLimitsFighterJetsLowerBoundMiddle() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"), 0);
        assertEquals("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 Kohav.check().", 0, aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundLeft() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"), -1);
        assertEquals("failure - hoursOfFlightSinceLastRepair = -1 shouldn't be reset to 0 Kohav.check().", 0, aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair());
    }


    @Test
    public void testCheckLimitsFighterJetsLowerBoundRight() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"), 1);
        assertEquals("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 Kohav.check().", 1, aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void testCheckLimitsFighterJetsUpperBoundRight() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"), 101);
        assertEquals("failure - hoursOfFlightSinceLastRepair = 101 should be reset to 0 Kohav.check().", 0, aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair());
    }
}