package AIF;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HaronCheckModuleExists {
    public static AIFUtil aifUtil;

    @BeforeClass
    public static void setUpBeforeClass() {
        aifUtil = new AIFUtil();
    }

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();
    }


    //קבוצות שקילות
    @Test//78
    public void testCheckHermesEquilibriumGroupsInGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Eitan"), 78);
        assertEquals("failure - hoursOfFlightSinceLastRepair = 84 shouldn't be reset to 0 after Eitan.check().", 78, aifUtil.getAerialVehiclesHashMap().get("Eitan").getHoursOfFlightSinceLastRepair());
    }


    @Test//299
    public void testCheckHermesEquilibriumGroupsAboveGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"), 173);
        assertEquals("failure - hoursOfFlightSinceLastRepair = 0 should  0 Shoval.check().", 0, aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair());
    }


    //ערכי גבול

    @Test
    public void testCheckLimitsFighterJetsLowerBoundMiddle() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Eitan"), 0);
        assertEquals("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 Eitan.check().", 0, aifUtil.getAerialVehiclesHashMap().get("Eitan").getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundLeft() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Eitan"), -1);
        assertEquals("failure - hoursOfFlightSinceLastRepair = -1 shouldn't be reset to 0 Eitan.check().", 0, aifUtil.getAerialVehiclesHashMap().get("Eitan").getHoursOfFlightSinceLastRepair());
    }


    @Test
    public void testCheckLimitsFighterJetsLowerBoundRight() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Eitan"), 1);
        assertEquals("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 Eitan.check().", 1, aifUtil.getAerialVehiclesHashMap().get("Eitan").getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void testCheckLimitsFighterJetsUpperBoundRight() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"), 151);
        assertEquals("failure - hoursOfFlightSinceLastRepair = 101 should be reset to 0 Shoval.check().", 0, aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair());
    }
}
